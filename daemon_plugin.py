#!/usr/bin/env python
#coding:utf-8

from glob import glob
from client import runMrtgClient
from validations_confs import runValidations, thereAreSettingInConf_d
from concurrent.futures import *
import os
import json

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def openConfigurations():
    configurations = []
    try:
        for file in glob(os.path.join(BASE_DIR,'sysgraph_client/conf.d/*.cfg')):
            args = json.loads(open(file,'r').read())
            configurations.append(args)
    except:
        pass
    return configurations
    
def runDaemonPlugin():
    error = []
    configurations = openConfigurations()
    error.append(thereAreSettingInConf_d(configurations))
    
    for config in configurations:
        error.append(runValidations(config))

    executador = ThreadPoolExecutor(max_workers=2)
    while (1 not in error):
        for config in configurations:
            result = executador.submit(runMrtgClient,config)
        result.result()
        
if __name__ == '__main__':
    runDaemonPlugin()
